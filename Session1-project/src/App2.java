
class Bike{
	//states
	public String color;
	public int wheel;
	
	//method or behaviour
	public void move() {
		System.out.println("the bicycle moves");
	}
}

class Customer{
	public String name;
	public int age;
	
	public void sayHi() {
		System.out.println("Hello");
	}
}

class Account{
	
}

public class App2{
	
	public static void main(String[] args) {
		//make an instance 
		//make an object
		
		Bike bicycle1 = new Bike();
		bicycle1.color = "Red";
		bicycle1.wheel = 2;
		bicycle1.move(); //the behavior
		
		
		Bike bicycle2 = new Bike();
		bicycle2.color = "Blue";
		bicycle2.wheel = 3;
		
		
		Customer customer1 = new Customer();
		customer1.name = "Reza";
		customer1.age = 30;
		customer1.sayHi();
		
		Customer customer2 = new Customer();
		customer2.name = "Betty";
		customer2.age = 20;

	}
}

