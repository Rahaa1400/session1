package jac;

//making the class by looking at
//class UML diagram
class Animal{
	//+age :int
	//+ => public
	public int age;
	public String gender;
	
	public void isMammal() {
		
	}
	
	public void mate() {
		
	}
}



class Clock{
	//Encapsulation
	private int hr;
	private int min;
	private int sec;
	
	//constructor
	
	//default constructor => it is implicitly in your class
//	public Clock() {
//		System.out.println("I am being called");
//	}
	
	//parametized constructor => I can make objects with the desired value
	//constructors they must NOT have any return type(void, int,...)
	public Clock(int hour, int minute, int second) {
		//hr = hour; => replace it with setter;
		setHr(hour);
		setMin(minute);
		setSec(second);
	}

	public int getHr() {
		return hr;
	}

	public void setHr(int hr) {
		if (hr >=0 && hr<=24) {
			//this means the current object
			this.hr = hr;	
		}
		else {
			System.out.println("you need to put hour between 0 and 24");
		}
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getSec() {
		return sec;
	}

	public void setSec(int sec) {
		this.sec = sec;
	}
	
	
	public void setTime(int h, int m, int s) {
		setHr(h);
		setMin(m);
		setSec(s);
	}
	
}

public class App3 {
	
	public static void main(String[] str) {
		//do some code
		
		//I make an object
		//Reference Variable
		Clock myClock = new Clock(0, 0, 0);
		//using getter to have access to the private fields
		int hour = myClock.getHr();
		
		Clock yourClock = new Clock(12, 14, 38);
		int minute = yourClock.getMin();

		Clock myFriendClock = new Clock(8, 25, 18);
	}
}
